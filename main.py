from kivy.app import App


class CalcApp(App):
    title = 'Calculatrice'

    def build(self):
        self.interface = self.root.ids['monlabel']

    def touch(self, val):
        self.interface.text += val

    def erase(self):
        self.interface.text = ""

    def erase1(self):
        self.interface.text = self.interface.text[:-1]

    def calc_result(self):
        try:
            self.interface.text = str(eval(self.interface.text))
        except:
            pass


CalcApp().run()
